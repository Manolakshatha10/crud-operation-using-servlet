package com.model;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public RegisterServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	
		 String Username= request.getParameter("name");
		 String Depart= request.getParameter("dept");
		 if(Regvalidate.validate(Username,Depart)) {
			 
			  PrintWriter pw= response.getWriter();
			  pw.print(" Valid Details and Valid User");
			  	 
		 }
		    
	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		 String Username= request.getParameter("name");
		 String Depart= request.getParameter("dept");
		 String  Age= request.getParameter("age");
		 String Dob=request.getParameter("dob");
		  Register re= new Register();
		  re.setUsername(Username);
		  re.setDept(Depart);
		  re.setAge(Age);
		  re.setDob(Dob);
		  
		  Regvalidate revalid= new Regvalidate();
		  revalid.getdetails(re);
		  
		  
		  RequestDispatcher  req= request.getRequestDispatcher("Login.html");
		  req.forward(request, response);
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		
		
	}

}
